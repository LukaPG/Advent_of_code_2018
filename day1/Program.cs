﻿using System;
using System.IO;
using System.Collections.Generic;

namespace task1{

    class Program{

        static int findFrequency(){
            var frequencies = new HashSet<int>();
            int frequency = 0;
            while(true){

                using(StreamReader sr = new StreamReader("task1_input.txt")){
                    string line;

                    while((line = sr.ReadLine()) != null){
                        int parsed = 0;
                        int.TryParse(line, out parsed);
                        frequency += parsed;
                        if(frequencies.Contains(frequency)){
                            return frequency;
                        }else{
                            frequencies.Add(frequency);
                        }
                    }
                } 
            }
        }
        static void Main(string[] args){
            Console.WriteLine(findFrequency());
        }
    }
}
