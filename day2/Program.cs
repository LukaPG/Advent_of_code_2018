﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

namespace task2
{
    class Program
    {
        /*
        Calculates checksum of a list of strings, by counting number of strings that have characters appearing twice and/or 
        characters appearing three times in them. 
        Checksum = numberOfStringsContaining3RepeatingChars * numberOfStringsContaining2RepeatingChars
         */
        static int calculateChecksum(){
            int idContaining2 = 0;
            int idContaining3 = 0;
            using(StreamReader sr = new StreamReader("input.txt")){
                string line;
                string alphabet = "abcdefghijklmnopqrstuvwxyz";
                while((line = sr.ReadLine()) != null){

                    bool found2 = false, found3 = false;
                    foreach(char c in alphabet){
                        int count = line.Count(chr => chr == c);
                        if(count == 2 && !found2){
                            Console.WriteLine("Char {0} has 2 occurences", c);
                            found2 = true;
                            idContaining2++;
                        }else if(count == 3 && !found3){
                            Console.WriteLine("Char {0} has 3 occurences", c);
                            found3 = true;
                            idContaining3++;
                        }
                    }
                }
            }
            Console.WriteLine("2s: {0}, 3s: {1}", idContaining2, idContaining3);
            return idContaining2*idContaining3;
        }

        static string ReplaceAt(string input, int index, char newChar){
            char[] chars = input.ToCharArray();
            chars[index] = newChar;
            return new string(chars);
        }

        /*
        Finds two strings that differ by only one character, by replacing each character in a string with a dummy one, 
        then adding it to a set. 
        When it finds a string in the set already, we know its found the string that differs by that one character that
        is replaced with the dummy. Returns sequence of matching characters.
         */
        static string findMatchingId(){
            var checkedIds = new HashSet<string>();

            using(StreamReader sr = new StreamReader("input.txt")){
                string line;
                while((line = sr.ReadLine()) != null){
                    for(int i=0; i<line.Length; i++){
                        
                        string modified = ReplaceAt(line, i, '_');
                        if (checkedIds.Contains(modified)){
                            return String.Join("", modified.Split("_"));
                        }
                        checkedIds.Add(modified);
                    }
                }
            }
            return null;
        }

        static void Main(string[] args)
        {
            Console.WriteLine(findMatchingId());
        }
    }
}
